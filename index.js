// MongoDB Aggregation
/*
        - Aggregation allows us to retrieve a group of data on specific conditions.
            in this case, we are retrieving or grouping the data inside our course_bookings
            table and getting the total count of all the dta inside of it.
*/

db.course_bookings.insertMany([
    {
        "courseId":  "C001", 
        "studentId": "S004", 
        "isCompleted": true
    },
    {
        "courseId":  "C002", 
        "studentId": "S001", 
        "isCompleted": false
    },
    {
        "courseId":  "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId":  "C003", 
        "studentId": "S002", 
        "isCompleted": false
    },
    {
        "courseId":  "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId":  "C004", 
        "studentId": "S004", 
        "isCompleted": false
    },
    {
        "courseId":  "C002", 
        "studentId": "S007", 
        "isCompleted": true
    },
    {
        "courseId":  "C003", 
        "studentId": "S005", 
        "isCompleted": false
    },
    {
        "courseId":  "C001", 
        "studentId": "S008", 
        "isCompleted": true
    },
    {
        "courseId":  "C004", 
        "studentId": "S0013", 
        "isCompleted": false
    }

]);

db.course_bookings.aggregate([
    {
        $group: { _id: null, count: { $sum: 1 } }
    }
]);

// $sum will act as a multiplier when use 2 and above.
// $match is a condition that has to be met in order for MongoDB to return data.
// In this case, we are trying to get all the fields where "isCompleted" 
db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $group: {_id: "$courseId", total: {$sum: 1}}
    }
]);

// $project basically either shows or doesn't show a field depending of you put 1 or 0 as its value.
db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $project: {"studentId": 1}
    }
]);

// $sort basically sorts the returned data in either ascending or descending order. (1 is ascending, -1 is descending).
db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $sort: {"courseId": -1}
    }
]);

// MINI ACTIVITY
/*
    1. Count the completed courses of student S013.
    2. Sort the courseId in descending order while studentId in ascending order.
*/
    db.course_bookings.aggregate([
        {$match: {"isCompleted": true}},
        {$sort: {"courseId": -1, "studentId": 1}}
    ]);

// 

    db.orders.insertMany([
        {
            "customer_Id": "A123",
            "amount": 500,
            "status": "A"
        },
        {
            "customer_Id": "A123",
            "amount": 250,
            "status": "A"
        },
        {
            "customer_Id": "A123",
            "amount": 200,
            "status": "A"
        },
        {
            "customer_Id": "A123",
            "amount": 200,
            "status": "D"
        }
    ]);


// Operators are a way to do automatic calc within our query.
/*
        1. $sum - gets the total of everything
        2. $max - gets the highest value out of everything else
        3. $min - gets the lowest value out of everything else
        4. $avg - gets the avg value of all the fields.
*/
    db.orders.aggregate([
        {
            $match: {"status": "A"}
        },
        {
            $group: {_id: "$customer_Id", minAmount: {$min: "$amount"}}
        }
    ]);